<?php

declare(strict_types=1);

namespace App\Actions;

use App\Http\Request\CompanyHttpRequest;
use App\Models\Company;
use Illuminate\Support\Facades\Auth;

final class AddCompanyAction
{


    public function execute(CompanyHttpRequest $request): Company
    {
        $company = new Company();
        $company->title = $request->getTitle();
        $company->description = $request->getDescription();
        $company->phone = $request->getPhone();
        $company->user_id = Auth::id();


        $company->save();

        return $company;
    }
}
