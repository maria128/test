<?php

declare(strict_types=1);

namespace App\Actions\Auth;

use App\Http\Request\Auth\RegisterHttpRequest;
use App\Models\User;

final class RegisterActions
{


    public function execute(RegisterHttpRequest $request): User
    {
        $user = new User;
        $user->first_name = $request->getFirstName();
        $user->last_name = $request->getLastName();
        $user->phone = $request->getPhone();
        $user->email = $request->getEmail();
        $user->password = app('hash')->make($request->getPassword());

        $user->save();

        return $user;
    }
}
