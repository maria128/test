<?php

declare(strict_types=1);

namespace App\Actions\Auth;

use App\Exceptions\TokenCouldNotCreate;
use App\Exceptions\UnauthenticatedException;
use App\Http\Request\Auth\LoginHttpRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

final class AuthenticatedUserAction
{
    public function execute(LoginHttpRequest $request): string
    {
        try {
            $token = JWTAuth::attempt([
                'email' => $request->getEmail(),
                'password' => $request->getPassword(),
            ]);

            if (!$token) {
                throw new UnauthenticatedException();
            }
        } catch (JWTException $exception) {
            throw new TokenCouldNotCreate();
        }

        return $token;
    }
}
