<?php

declare(strict_types = 1);

namespace App\Actions\Auth;

use App\Exceptions\TokenCouldNotCreate;
use App\Exceptions\UserByEmailNotFoundException;
use App\Http\Request\Auth\ResetPasswordHttpRequest;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


final class SendResetPasswordLinkAction
{

    public function execute(ResetPasswordHttpRequest $request): string
    {
        $user = User::where('email',  $request->get('email'))->first();

        if (!$user) {
            throw new UserByEmailNotFoundException();
        }

        try {
            JWTAuth::factory()->setTTL(30);
            $token = JWTAuth::fromUser($user);
        } catch (JWTException $e) {
            throw new TokenCouldNotCreate();
        }


        return $token;
    }
}
