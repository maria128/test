<?php

namespace App\Http\Controllers;

use App\Actions\AddCompanyAction;
use App\Http\Request\CompanyHttpRequest;
use App\Models\Company;
use Illuminate\Support\Facades\Auth;


class CompanyController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function addCompany(CompanyHttpRequest $request)
    {
        try {
            return response()->json(['company' =>
                (new AddCompanyAction())->execute($request), 'message' => 'CREATED'], 201);

        } catch (\Exception $e) {

            return response()->json(['message' => $e->getMessage()], 409);
        }

    }

    public function allCompanies()
    {
        return response()->json(['companies' =>  Company::where('user_id', Auth::id())->get()], 200);
    }

}
