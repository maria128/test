<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\Auth\SendResetPasswordLinkAction;
use App\Http\Request\Auth\ResetPasswordHttpRequest;


final class ResetPasswordController extends Controller
{

    public function sendPasswordResetLink(ResetPasswordHttpRequest $request)
    {
        try {
            return $this->respondWithToken((new SendResetPasswordLinkAction())->execute($request));

        } catch (\Exception $e) {

            return response()->json(['message' => $e->getMessage()]);
        }
    }

}
