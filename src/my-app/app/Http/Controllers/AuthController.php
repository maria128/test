<?php

namespace App\Http\Controllers;

use App\Actions\Auth\AuthenticatedUserAction;
use App\Actions\Auth\RegisterActions;
use App\Http\Request\Auth\LoginHttpRequest;
use App\Http\Request\Auth\RegisterHttpRequest;

class AuthController extends Controller
{

    public function register(RegisterHttpRequest $request)
    {
        try {
            return response()->json(['user' =>
                (new RegisterActions())->execute($request), 'message' => 'CREATED'], 201);

        } catch (\Exception $e) {

            return response()->json(['message' => $e->getMessage()], 409);
        }
    }


    public function login(LoginHttpRequest $request)
    {
        try {
            return $this->respondWithToken((new AuthenticatedUserAction())->execute($request));

        } catch (\Exception $e) {

            return response()->json(['message' => $e->getMessage()]);
        }
    }
}
