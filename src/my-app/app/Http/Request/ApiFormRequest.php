<?php

declare(strict_types = 1);

namespace App\Http\Request;



abstract class ApiFormRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }
}
