<?php

declare(strict_types=1);

namespace App\Http\Request\Auth;

use App\Http\Request\ApiFormRequest;

final class LoginHttpRequest extends ApiFormRequest
{

    public function getEmail()
    {
        return $this->get('email');
    }

    public function getPassword()
    {
        return $this->get('password');
    }

    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required|min:8|string',
        ];
    }
}
