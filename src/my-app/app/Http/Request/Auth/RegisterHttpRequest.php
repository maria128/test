<?php

declare(strict_types=1);

namespace App\Http\Request\Auth;

use App\Http\Request\ApiFormRequest;

final class RegisterHttpRequest extends ApiFormRequest
{

    public function getFirstName()
    {
        return $this->get('first_name');
    }

    public function getLastName()
    {
        return $this->get('last_name');
    }

    public function getPhone()
    {
        return $this->get('phone');
    }

    public function getEmail()
    {
        return $this->get('email');
    }

    public function getPassword()
    {
        return $this->get('password');
    }

    public function rules(): array
    {
        return [
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|string',
        ];
    }
}
