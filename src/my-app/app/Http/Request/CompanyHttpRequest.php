<?php

declare(strict_types=1);

namespace App\Http\Request;

final class CompanyHttpRequest extends ApiFormRequest
{

    public function getTitle()
    {
        return $this->get('title');
    }

    public function getDescription()
    {
        return $this->get('description');
    }

    public function getPhone()
    {
        return $this->get('phone');
    }

    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'phone' => 'required|string',
        ];
    }
}
