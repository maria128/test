<?php

declare(strict_types=1);

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Visitor
 * @package App\Entities
 * @property int $id
 * @property string $visitor_type
 * @property int $website_id
 * @property Carbon $last_activity
 */
final class Company extends Model
{
    protected $fillable = [
        'title',
        'phone',
        'description',
        'user_id'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
