<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/





Route::group([
    'prefix' => 'api/user',
], function () {
    Route::post('/register', 'AuthController@register');
    Route::post('/sign-in', 'AuthController@login');
    Route::post('/recover-password', 'ResetPasswordController@sendPasswordResetLink');
});

Route::group([
    'prefix' => 'api/user',
], function () {
    Route::get('/companies', 'CompanyController@allCompanies');
    Route::post('/companies', 'CompanyController@addCompany');
});
