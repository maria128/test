# Test project Get Companies Auth user

## Running the Containers
Start containers with:
```
docker-compose up -d
```

Stop containers with:
```
docker-compose stop
```

## Create the Lumen Application
Open shell to docker container
```
docker exec -it lumen /bin/bash
```
  
## upload the application
```
composer update
```

## Make sure lumen is running

Endpoints:
```
http://localhost/my-app/public/api/user/register

POST

body {
first_name: test,
last_name: test,
phone:122565,
email:test@gmail.com,
password:test
}

Output 
{"user":{"first_name":"test","last_name":"test","phone":"122565","email":"test@gmail.com","updated_at":"2022-02-12T08:45:53.000000Z","created_at":"2022-02-12T08:45:53.000000Z","id":2},"message":"CREATED"}


http://localhost/my-app/public/api/user/sign-in

POST
body {
email:test@gmail.com,
password:test
}

Output 
{"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL215LWFwcFwvcHVibGljXC9hcGlcL3VzZXJcL3NpZ24taW4iLCJpYXQiOjE2NDQ2NTU1NjksImV4cCI6MTY0NDY1OTE2OSwibmJmIjoxNjQ0NjU1NTY5LCJqdGkiOiI3YW1KOGtKTzZyY2sydlRuIiwic3ViIjoyLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.YksPF9MaEWZryiUoiI8mcFFPH7TbwCjDFvVyZUZYJ8Y","token_type":"bearer","expires_in":3600}


http://localhost/my-app/public/api/user/recover-password

POST
body {
email:test@gmail.com
}

Output 
{"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL215LWFwcFwvcHVibGljXC9hcGlcL3VzZXJcL3NpZ24taW4iLCJpYXQiOjE2NDQ2NTU1NjksImV4cCI6MTY0NDY1OTE2OSwibmJmIjoxNjQ0NjU1NTY5LCJqdGkiOiI3YW1KOGtKTzZyY2sydlRuIiwic3ViIjoyLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.YksPF9MaEWZryiUoiI8mcFFPH7TbwCjDFvVyZUZYJ8Y","token_type":"bearer","expires_in":3600}


http://localhost/my-app/public/api/user/companies

GET


Output 
{"companies":[]}


http://localhost/my-app/public/api/user/companies

POST
body {
title:test
description:test
phone:125469
}

Output 
{"company":{"title":"test","description":"test","phone":"125469","user_id":1,"updated_at":"2022-02-12T08:57:57.000000Z","created_at":"2022-02-12T08:57:57.000000Z","id":3},"message":"CREATED"}

```